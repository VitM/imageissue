## Module #2 - So You Wanna Be A Developer... (Required)

Well you've come to the right place. Over the course of the next six months you'll be undertaking a rigorous training to develop the skills necessary to enter the field of web and software development. 

But you may be wondering: *"What exactly is a web developer?"*

Great question. For many of us, the idea of web developer is one that evokes vague notions of sketching designs, creating websites, and wearing hipster clothing. But the day-to-day work of web development remains a mystery. To better grasp the answer, it's worth taking a step back to understand how the Internet works.

### How the Internet Works in 50 seconds...

Most of us take for granted that when we click a link online -- *something* happens. Maybe a chat window pops up... Maybe a video loads... Or maybe we end up on a new page completely. 

This "something" happens seamlessly, despite the Internet being composed of hundreds of thousands of loosely connected networks of machines and software. In fact, each time you "visit" a webpage -- you are actually "communicating" with one of these networks. The network then recognizes your *request* and in-turn *responds* with a viewable webpage. 

In the old-day, this was the end of the story. Viewable webpages were simple documents composed of a little code describing how they should look and act, and, because the pages were static, the transmission was a simple one-way street. (A great example of a "static" website would be a simple Geocities page).

![Geocities Webpage](Images/Geocities.png)

But as time went on, the demands and expectations of users continually increased. Users were hungry for websites that felt live, responsive, and richly interactive. Because of this, new technologies advanced such that more complex code could be embedded into a webpage's makeup. 

Instead of a network being restricted to sending a "static" webpage, it became possible to send dynamic websites capable of doing much more. For example, the modern website (or... *web application* as they are called) is capable of doing any of the following:

* Displaying real-time data to users continually
* Responding to multiple user interface elements simultaneously
* Synchronizing visual elements between users across locations 
* Live streaming video seamlessly
* Maintaining encrypted channels for communicating private information
* Algorithmically predicting user preferences based on historic choices 
* And much more!

![Facebook Webpage](./Images/Facebook.png)

Because of their added complexity, however, these features require a broad expertise of a variety of technologies and programming methods. As experts in these technologies and methods, *web developers* thus play the essential role of being the architects behind users' web experience. 

### So, is it sort of like... Graphic Design?

Unfortunately, not really. The tools used to architect the web are far from the point in which developers can simply "drag and drop" visual components onto the screen. Instead developers use specialized languages like HTML, CSS, and Javascript to lay out their designs and functional features in code. This makes the process more challenging for beginners, but at the same -- is the reason every browser on the web can view webpages in the same way. 

![Code Layout](CodeLayout.png)

### Sounds Technical... Do I Need to Have a Computer Science Background?

Absolutely not! In fact, according to some studies, nearly 50% of all professional developers are self-taught. Think about that for a moment. Today, there are few skills that you can learn right now that present the breadth of job opportunities, the quality of job opportunities, and the creative freedom that come with web development. Yet, unlike most industries, the field isn't restricted to just those with a formal degree. It's a powerful opportunity for those looking to make a career change or build a new skill. 

### If I don't need a degree... How do I prove I'm actually a developer?

Another great question! If you remain hardworking, diligent, and focused on learning -- you'll be able to prove your professional expertise by having a portfolio of works. For many of you, this will be a liberating feeling. Instead of being judged on the basis of your past credentials or work experiences, your actual *works* will become the primary gateway to your future career.    

For this reason, throughout the program, you will be constantly tasked  with building web applications for your homework assignments and projects. Take these seriously! They will often feel very challenging -- but swing with the punches and try your hardest on each assignment. If you stay consistent and remain confident, your work will *gradually* become better and you will have the proof you need to demonstrate your skills for the professional arena. 

### I'm still a bit fuzzy. 

Understandable. Take a stab at the next assignment and get one step closer to clarity. 

-------

### Assignment (Required): 
* [My Future Job... I think?](Assignment.md)

### Additional Reading / Viewing:
* [What Does a Web Developer Do?](https://www.youtube.com/watch?v=GEfuOMzRgXo)
* [How Do I Compete as a New Web Developer?](https://www.youtube.com/watch?v=TkKeFbX76rY)

-------

### Copyright 
Coding Boot Camp (C) 2016. All Rights Reserved.




